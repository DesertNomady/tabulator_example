Task.create(task_ID: 3, title: 'Create project plan', initials: 'ABCD', project: 'Project Snowflake', 
    category: 'Cat One', task_type: 'Type One', output: 'Documentation', description: 'Make project plan with stakeholders',
    date_received: '2018-01-01', due_date: '2019-01-01', requester: 'QWER', status: 'Open', progress: '10')
    
Task.create(task_ID: 7, title: 'Update project plan', initials: 'ABCD', project: 'Project Snowflake', 
    category: 'Cat One', task_type: 'Type One', output: 'Documentation', description: 'MakeUpdate project plan with other stakeholders',
    date_received: '2018-01-01', due_date: '2019-01-01', requester: 'QWER', status: 'Open', progress: '25')    

Task.create(task_ID: 9, title: 'Write validation plan', initials: 'HJKKL', project: 'Project Snowflake', 
    category: 'Cat One', task_type: 'Type Two', output: 'Documentation', description: 'MakeUpdate project plan with other stakeholders',
    date_received: '2018-01-01', due_date: '2019-01-01', requester: 'QWER', status: 'Open', progress: '50')
    
Task.create(task_ID: 10, title: 'Make coffee', initials: 'HJKKL', project: 'Project Snowflake', 
    category: 'Cat One', task_type: 'Type Two', output: 'Documentation', description: 'qwert',
    date_received: '2018-01-01', due_date: '2019-01-01', requester: 'QWER', status: 'Cancelled', progress: '')
    
Task.create(task_ID: 11, title: 'Buy snacks', initials: 'HJKKL', project: 'Project Snowflake', 
    category: 'Cat One', task_type: 'Type Two', output: 'Documentation', description: 'qwert',
    date_received: '2018-01-01', due_date: '2019-01-01', requester: 'QWERTY', status: 'Completed', progress: '100')            