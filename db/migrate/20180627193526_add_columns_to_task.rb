class AddColumnsToTask < ActiveRecord::Migration[5.2]
  def change
    add_column :tasks, :initials, :string
    add_column :tasks, :project, :string
    add_column :tasks, :category, :string
    add_column :tasks, :task, :string
    add_column :tasks, :output, :string
    add_column :tasks, :title, :string
    add_column :tasks, :description, :string
    add_column :tasks, :date_received, :datetime
    add_column :tasks, :due_date, :datetime
    add_column :tasks, :delivery_date, :datetime
    add_column :tasks, :rev_due_date, :datetime
    add_column :tasks, :rev_date_reason, :string
    add_column :tasks, :is_cancelled, :boolean, :default => 0
  end
end
