class TasksController < ApplicationController
  def index
    @tasks = Task.all
    @task = Task.first
    @tasktest = Task.first
    @incomplete_tasks = Task.where(complete: false)
    @complete_tasks = Task.where(complete: true)
    
    
  end

  def new
    @task = Task.new
  end

def create
  
  
  @task = Task.create!(allowed_params)

  respond_to do |f|
    f.html { redirect_to tasks_url }
    f.js
  end
end

def update
  
  @task = Task.find(params[:id])
  @task.update_attributes!(allowed_params)
  
  
  respond_to do |f|
    f.html { redirect_to tasks_url }
    f.js
  end
end
  
def destroy
  @task = Task.destroy(params[:id])

  respond_to do |f|
    f.html { redirect_to tasks_url }
    f.js
  end
end

def tabEdit
  @task = Task.find(params[:id])

  respond_to do |f|
    f.html
    f.js
  end  


end

  def show
    @task = Task.find(params[:id])
 
    respond_to do |format|
      format.html
      format.js { render layout: false, content_type: 'text/javascript' }
    end
  end

  private

  def allowed_params
    params.require(:task).permit(:id, :title, :name, :complete, :date_received, :project, :delivery_date, :due_date, :requester, :category, :task_type, :output, :description)
  end
end