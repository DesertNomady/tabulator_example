Rails.application.routes.draw do
  get 'tabedit/:id', to: 'tasks#tabedit'
  
  resources :tasks

  root to: 'tasks#index'

end
